var reader;
var progress = $('.percent');

function abortRead() {
    reader.abort();
}

function errorHandler(evt) {
    switch(evt.target.error.code) {
      case evt.target.error.NOT_FOUND_ERR:
        alert('File Not Found!');
        break;
      case evt.target.error.NOT_READABLE_ERR:
        alert('File is not readable');
        break;
      case evt.target.error.ABORT_ERR:
        break; // noop
      default:
        alert('An error occurred reading this file.');
    };
}

function updateProgress(evt) {
    // evt is an ProgressEvent.
    if (evt.lengthComputable) {
      var percentLoaded = Math.round((evt.loaded / evt.total) * 100);
      // Increase the progress bar length.
      if (percentLoaded < 100) {
        progress.width = percentLoaded + '%';
        progress.textContent = percentLoaded + '%';
      }
    }
}

function handleFileSelect(evt) {
    var inputFile = evt.target.files[0];
    
    // Reset progress indicator on new file selection.
    progress.width = '0%';
    progress.textContent = '0%';

    reader = new FileReader();
    reader.onerror = errorHandler;
    reader.onprogress = updateProgress;
    reader.onabort = function(e) {
      alert('File read cancelled');
    };
    reader.onloadstart = function(e) {
      $('#progress_bar').className = 'loading';
    };
    reader.onload = function(e) {
      // Ensure that the progress bar displays 100% at the end.
      progress.width = '100%';
      progress.textContent = '100%';
      setTimeout("document.getElementById('progress_bar').className='';", 2000);
    }

    reader.onloadend = function(e) {
        var output = [];
        output.push('&nbsp;&nbsp;<strong>File loaded: </strong>', escape(inputFile.name), '(', 
              inputFile.type || 'n/a', ') - ',
              inputFile.size, ' bytes, last modified: ',
              inputFile.lastModifiedDate ? inputFile.lastModifiedDate.toLocaleDateString() : 'n/a');
        $('#uploadedFile').html(output.join(''));

        var userJSONdata = e.target.result;
        clearAll();
        hideCenter();
        storeAllWheelDataFromJsonDataArray($.parseJSON(userJSONdata), displayAllWheels);
    }

    // Read in the file as a binary string.
    reader.readAsBinaryString(inputFile);
}
